package com.jvs.softclinic.commonmodule;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Response;
import com.jvs.softclinic.model.EntityMapping;
import com.jvs.softclinic.model.GenericResponse;
import com.jvs.softclinic.model.TestCase;

import static com.jayway.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;

public class TestAutomation {
	static JsonParser parser = new JsonParser();

	public static void get(TestCase testCase) {
		System.out.println("---------------------------------------------------------------------------------");
		System.out.println("Get Request : \n"+testCase);

		String entity = testCase.getMeta().getEntity().toLowerCase();
		String url = EntityMapping.getMappings().get(entity).get("url");
		String id  = testCase.getData().get("id").getAsString();
		JsonObject expectedResponse = testCase.getResponse().getAsJsonObject("responseObject");

		System.out.print("Expected : "+expectedResponse);

		Response actualResponse = given()
			.log().all().header(new Header("Authorization","Bearer "+RunTestCase.getAccessToken()))
				.header(new Header("Content-Type","application/json"))
			.when().get(url+id);

		GenericResponse receivedResponse = new GenericResponse(actualResponse.getStatusCode(),actualResponse.getBody().asString());

		if(receivedResponse.getStatusCode() == Integer.parseInt(String.valueOf(testCase.getResponse().get("statusCode")))){
			System.out.println("Get response : Status Matched");
			/*try {

				String expectedString = expectedResponse.get("responseObject").getAsString();
				JSONAssert.assertEquals(expectedString,
						receivedResponse.getResponseObject(), JSONCompareMode.LENIENT);
			}catch (AssertionError e){
				System.out.println("*********AssertionError while comparing json:"+e);
			}catch (JSONException e1){
				System.out.println("*********JSONException while comparing json:"+e1);
			}*/
		}else {
			System.out.println("Get response : Status not matched");
			System.out.println("Expected Response code: " + testCase.getResponse().get("statusCode") +
					" Got: " + actualResponse.getStatusCode());
		}

		assertEquals(parser.parse(receivedResponse.getResponseObject()),parser.parse(expectedResponse.toString()));

	}
	
	public static void post(TestCase testCase){
		System.out.println("---------------------------------------------------------------------------------");

		System.out.println("Post case : \n"+testCase);
		String entity = testCase.getMeta().getEntity().toLowerCase();
		String url = EntityMapping.getMappings().get(entity).get("url");
		JsonObject reqBody = testCase.getData();
		JsonObject expectedResponse = testCase.getResponse().getAsJsonObject("responseObject");
		System.out.println("Expected response : "+expectedResponse);
		Response actualResponse = given()
			.log().all().header(new Header("Authorization","Bearer "+RunTestCase.getAccessToken()))
			.contentType("application/json")
			.body(reqBody)
			.when().post(url);

		GenericResponse receivedResponse = new GenericResponse(actualResponse.getStatusCode(),actualResponse.getBody().asString());
		if(receivedResponse.getStatusCode() == Integer.parseInt(testCase.getResponse().get("statusCode").toString())){
			System.out.println("Post response : Status Matched");
		}else {
			System.out.println("Post response : Status not matched");
			System.out.println("Expected Response code: " + testCase.getResponse().get("statusCode") +
					" Got: " + receivedResponse.getStatusCode());
		}

		assertEquals(parser.parse(receivedResponse.getResponseObject()),parser.parse(expectedResponse.toString()));

	}

	public static void put(TestCase testCase){
		System.out.println("---------------------------------------------------------------------------------");

		System.out.println("Put case : \n"+testCase);
		String entity = testCase.getMeta().getEntity().toLowerCase();
		String url = EntityMapping.getMappings().get(entity).get("url");
		String id  = testCase.getData().get("id").getAsString();
		JsonObject reqBody = testCase.getData();
		JsonObject expectedResponse = testCase.getResponse().getAsJsonObject("responseObject");

		Response actualResponse = given()
				.log().all().header(new Header("Authorization","Bearer "+RunTestCase.getAccessToken()))
				.contentType("application/json")
				.body(reqBody)
				.when().put(url+id);

		GenericResponse receivedResponse = new GenericResponse(actualResponse.getStatusCode(),actualResponse.getBody().asString());

		if(receivedResponse.getStatusCode() == Integer.parseInt(String.valueOf(testCase.getResponse().get("statusCode")))){
			System.out.println("Put response : Status Matched");
		}else {
			System.out.println("Put response : Status not matched");
			System.out.println("Expected Response code: " + testCase.getResponse().get("statusCode") + " Got: " + actualResponse.getStatusCode());
		}

		assertEquals(parser.parse(receivedResponse.getResponseObject()),parser.parse(expectedResponse.toString()));
	}

	public static void delete(TestCase testCase){
		System.out.println("---------------------------------------------------------------------------------");

		System.out.println("Delete Request : \n"+testCase);
		String entity = testCase.getMeta().getEntity().toLowerCase();
		String url = EntityMapping.getMappings().get(entity).get("url");
		String id     = testCase.getData().get("id").getAsString();
		JsonObject expectedResponse = testCase.getResponse().getAsJsonObject("responseObject");

		Response actualResponse = given()
				.log().all().header(new Header("Authorization","Bearer "+RunTestCase.getAccessToken()))
				.header(new Header("Content-Type","application/json"))
				.when().delete(url+id);

		GenericResponse receivedResponse = new GenericResponse(actualResponse.getStatusCode(),
				actualResponse.getBody().asString());

		if(receivedResponse.getStatusCode() == Integer.parseInt(String.valueOf(testCase.getResponse().get("statusCode")))){
			System.out.println("Delete response : Status Matched");
		}else {
			System.out.println("Delete response : Status not matched");
			System.out.println("Expected Response code: " + testCase.getResponse().get("statusCode") + " Got: " + actualResponse.getStatusCode());
		}

		assertEquals(parser.parse(receivedResponse.getResponseObject()),parser.parse(expectedResponse.toString()));

	}
	
}
