package com.jvs.softclinic.commonmodule;

import static com.jayway.restassured.RestAssured.rootPath;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jayway.restassured.RestAssured;
import com.jvs.softclinic.constants.APIConstants;
import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import com.jayway.restassured.path.json.JsonPath;
import com.jvs.softclinic.FunctionalTest;
import com.jvs.softclinic.model.EntityMapping;
import com.jvs.softclinic.model.TestCase;
import com.jvs.softclinic.model.TestScenario;
import com.jvs.softclinic.model.TestScenarioDetails;
//import org.junit.jupiter.api.Test;

public class RunTestCase extends FunctionalTest {
	private static String ACCESS_TOKEN;
	private static String REFRESH_TOKEN;

	private static final String RESOURCES = "resources/";
	private static final String ENTITY_MAPPING = "resources/entitymapping.json";
	private static final String TEST_SCENARIOS = RESOURCES+"/testscenarios";

//	@BeforeClass
//	public static void getEntityMapping(){
	static{
		File jsonExample = new File(System.getProperty("user.dir"),ENTITY_MAPPING);
		Map<String,Map<String,String>> mapping = new JsonPath(jsonExample).getMap(rootPath);
		EntityMapping.setMappings(mapping);
		System.out.println("Mapping: "+mapping);
	}

	public static String getAccessToken() {
		return ACCESS_TOKEN;
	}

	public static void setAccessToken(String accessToken) {
		ACCESS_TOKEN = accessToken;
	}

	public static String getRefreshToken() {
		return REFRESH_TOKEN;
	}

	public static void setRefreshToken(String refreshToken) {
		REFRESH_TOKEN = refreshToken;
	}

	@Test
	public void testScenarios(){
		File[] testScenarioDir = new File(System.getProperty("user.dir"), TEST_SCENARIOS).listFiles();
		Set<File> scenariosFileSet = new TreeSet<>();
		//Prepare Test Scenario List
		for(int i=0;i<testScenarioDir.length;i++){
			File directory = testScenarioDir[i];
			if(directory.isDirectory()){
				File[] files = directory.listFiles();
				for(int j=0;j<files.length;j++){
					TestScenario testScenario = new JsonPath(files[j]).getObject(rootPath,TestScenario.class);
					processScenario(testScenario, scenariosFileSet);
				}
			}
		}
		System.out.println("\n\nFinal File List: "+scenariosFileSet);
		
		//Execute Test Scenario List
		Iterator<File> files = scenariosFileSet.iterator();
		while(files.hasNext()){
			File file = files.next();
			System.out.println("\n\nExecuting Test Case: "+file+"\n");
			processFile(file);
		}
	}
	
	private void processScenario(TestScenario testScenario, Set<File> scenariosFileSet){
		
		for(int i=0;i<testScenario.getTestCases().length;i++){
			TestScenarioDetails testCase = testScenario.getTestCases()[i];
			switch(testCase.getType().toUpperCase()){
				case "TESTCASE":
					System.out.println("Adding TestCase: "+testCase.getFile());
					scenariosFileSet.add(new File(testCase.getFile()));
					break;
				case "TESTSCENARIO":
					System.out.println("Adding TestScenario: "+testCase.getFile());
					TestScenario tmpScenario = new JsonPath(new File(System.getProperty("user.dir"),testCase.getFile())).getObject(rootPath, TestScenario.class);
					processScenario(tmpScenario, scenariosFileSet);
					break;
			}
		}
	}
	
	private void processFile(File file){
		//TODO Different Wrapper Class for each testing
		TestCase wrapper = new JsonPath(file).getObject(rootPath, TestCase.class);

		try{
			switch(wrapper.getMeta().getOperation().toUpperCase()){
				case "GET":
					TestAutomation.get(wrapper);
					break;
				case "POST":
					System.out.println("Token::"+getAccessToken());
					TestAutomation.post(wrapper);
					break;
				case "PUT":
					TestAutomation.put(wrapper);
					break;
				case "DELETE":
					TestAutomation.delete(wrapper);
					break;
			}
		}catch(Error exp){
			System.out.println("Error: "+wrapper.getMeta().getTestCaseId()+" "+exp.getMessage());
		}
	}
	
}
