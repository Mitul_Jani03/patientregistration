package com.jvs.softclinic.model;

import java.util.Arrays;

public class TestScenario {
	
	private TestScenarioDetails[] testCases;

	public TestScenarioDetails[] getTestCases() {
		return testCases;
	}

	public void setTestCases(TestScenarioDetails[] testCases) {
		this.testCases = testCases;
	}

	@Override
	public String toString() {
		return "TestScenario [testCases=" + Arrays.toString(testCases) + "]";
	} 
	
}
