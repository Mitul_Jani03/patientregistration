package com.jvs.softclinic.model;

import java.util.Map;

public class EntityMapping {
	
	private static Map<String,Map<String,String>> mappings;

	public static Map<String, Map<String,String>> getMappings() {
		return mappings;
	}

	public static void setMappings(Map<String, Map<String,String>> mappings) {
		EntityMapping.mappings = mappings;
	}
	
}
