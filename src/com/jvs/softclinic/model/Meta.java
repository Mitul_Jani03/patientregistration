package com.jvs.softclinic.model;

public class Meta {
    private String testCaseId;
	private String entity;
	private String operation;
	private String date;
	public String getTestCaseId() {
		return testCaseId;
	}
	public void setTestCaseId(String testCaseId) {
		this.testCaseId = testCaseId;
	}
	public String getEntity() {
		return entity;
	}
	public void setEntity(String entity) {
		this.entity = entity;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	@Override
	public String toString() {
		return "Meta [testCaseId=" + testCaseId + ", entity=" + entity + ", operation=" + operation + ", date=" + date
				+ "]";
	}	
}
