package com.jvs.softclinic.model;

import com.google.gson.JsonParser;
import com.jayway.restassured.response.ResponseBody;

public class GenericResponse {
    private int statusCode;
    private String responseObject;

    public GenericResponse(int statusCode, String responseObject) {
        this.statusCode = statusCode;
        this.responseObject = responseObject;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getResponseObject() {
        return responseObject;
    }

    public void setResponseObject(String responseObject) {
        this.responseObject = responseObject;
    }

    public boolean compareResponseJSONs(String expected,String received){
        JsonParser parser = new JsonParser();

        if(parser.parse(expected).equals(parser.parse(received))) {
            return true;
        }

        return false;
    }
}
